//
// Created by Diego Diaz on 2018-12-23.
//
#include "gtest/gtest.h"
#include <boss.hpp>
#ifdef WINDOWS
#include <direct.h>
    #define GetCurrentDir _getcwd
#else
#include <unistd.h>
#define GetCurrentDir getcwd
#endif

extern "C"{
#include "kseq.h"
#include <zlib.h>
}

#include <sdsl/sd_vector.hpp>
#include <sdsl/suffix_arrays.hpp>

#include <iostream>
#include <vector>
#include <random>
#include "ef_array.hpp"
#include "color_matrix.hpp"

KSEQ_INIT(gzFile, gzread);

class BOSSTests : public testing::Test{


    void SetUp(){
        if (!GetCurrentDir(curr_path, sizeof(curr_path))) exit(1);
        curr_path[sizeof(curr_path) - 1] = '\0';
        file = std::string(curr_path)+"/../../tests/test_data.fastq";
        size_t k=10;
        dbg_boss dbg(file, k, 4);
        std::cout<<dbg.tot_colors()<<" colors were generated "<<std::endl;
        sdsl::store_to_file(dbg, dbg_file);
        load_from_file(my_dbg, dbg_file);
    }

public:
    dbg_boss my_dbg;
    std::string file;
    char curr_path[FILENAME_MAX];
    std::string dbg_file="my_dbg.boss";

    void TearDown(){
        remove(dbg_file);
    }
};

TEST_F(BOSSTests, elias_fano){

    std::random_device rd;
    std::mt19937 rng(rd());
    std::uniform_int_distribution<uint16_t> dist(1, 2000);
    std::vector<uint16_t> random_vec;
    for(size_t i=0;i<1000000;i++){
        random_vec.push_back(dist(rng));
    }
    ef_array<std::vector<uint16_t>> ef(random_vec);

    for(size_t i=0;i<1000000;i++){
        ASSERT_EQ(ef.read_elm(i), random_vec[i]);
    }

    /*sdsl::int_vector_buffer<16> f_colors_list_buffer("f_color_list_146929_0.sdsl",
                                                     std::ios::in,
                                                     1024*1024,
                                                     false);
    ef_array<sdsl::int_vector_buffer<16>> ef2(f_colors_list_buffer);
    for(size_t i=0;i<f_colors_list_buffer.size();i++){
        ASSERT_EQ(ef2.read_elm(i), f_colors_list_buffer[i]);
    }*/
}

TEST_F(BOSSTests, color_matrix){

    sdsl::bit_vector row_bv={0,1,0,1,1,1,1,1,0,0,1,0,1,0,0,1,1,1,0};
    sdsl::bit_vector col_bv={1,0,1,0,0,1,1,1,0,0,1,1,0,1,0,1,0,1,0,0,1,0,1};
    std::vector<uint16_t> colors={1,4,2,1,3,4,1,1,3,2,6,2,2,3,3,1,4,2,1,1,4,1};

    std::vector<std::vector<uint16_t>> matrix = {{},{1,5},{},{2,3,6},{4},{1},
                                                 {1,4,6},{6},{},{},{2,4},{},
                                                 {3,6},{},{},{1,5},{2,3,4},{4,5}};

    color_matrix<std::vector<uint16_t>, sdsl::bit_vector> cm(colors, row_bv, col_bv);
    color_matrix<std::vector<uint16_t>, sdsl::bit_vector> cm2;

    cm2.swap(cm);

    for(size_t i=0;i<matrix.size();i++){
        std::vector<uint32_t> cols = cm2.get_colors(i);
        for(size_t j=0;j<cols.size();j++){
            ASSERT_EQ(cols[j], matrix[i][j]);
        }
    }

    /*sdsl::bit_vector f_colors_marks;
    sdsl::load_from_file(f_colors_marks,
                         "f_color_marks_147753_0.sdsl");

    sdsl::bit_vector color_marks_tmp;
    sdsl::load_from_file(color_marks_tmp,
                         "color_marks_147753_0.sdsl");

    sdsl::int_vector_buffer<16> f_colors_list_buffer("f_color_list_147753_0.sdsl");

    std::cout<<f_colors_list_buffer.size()<<" "<<color_marks_tmp.size()<<" "<<f_colors_marks.size()<<std::endl;

    for(size_t i=106403;i<(106403)+4;i++){
        std::cout<<f_colors_list_buffer[i]<<" ";
    }
    std::cout<<" "<<std::endl;

    //store the colors succinctly
    color_matrix<sdsl::int_vector_buffer<16>, sdsl::bit_vector> cm3(f_colors_list_buffer,
                                                                    color_marks_tmp,
                                                                    f_colors_marks);
    std::cout<<cm3.get_colors(70224)<<std::endl;*/
}

TEST_F(BOSSTests, colors){

    //THIS TEST WILL PASS ONLY WITH "test_data.fm_index" and its fm_index!!!

    size_t outd;
    dbg_boss::size_type first_node, tmp_node, neighbor, cand_node;
    size_t n_colors=0, tot_colors=0;

    load_from_file(my_dbg, dbg_file);

    //FM index of the test data to test the correctness of the get_colors
    sdsl::csa_wt<wt_huff<rrr_vector<127> >, 512, 1024> fm_index;
    std::string fm_index_file = std::string(curr_path)+"/../../tests/test_data.fm_index";
    sdsl::load_from_file(fm_index, fm_index_file);

    std::string kmer_seq;
    size_t fm_count, acc_fm_counts=0, n_ambigous=0;
    std::unordered_map<dbg_boss::size_type, bool> nodes_seen;

    for(size_t i=1;i<=my_dbg.n_solid_nodes;i++){

        tmp_node = my_dbg.solid_nodes_ss(i);
        first_node = my_dbg.incomming(tmp_node,'$',true);

        if(first_node!=0 && nodes_seen.count(first_node)==0){

            nodes_seen[first_node] = true;

            //checking that the number of colors is ok
            kmer_seq = my_dbg.node2string(first_node);
            for(auto & sym : kmer_seq) sym = dna_alphabet::char2comp[sym];
            fm_count = sdsl::count(fm_index,kmer_seq);

            std::reverse(kmer_seq.begin(), kmer_seq.end());
            for(auto& sym: kmer_seq)sym = dna_alphabet::comp2rev[sym];
            fm_count += sdsl::count(fm_index,kmer_seq);

            std::vector<uint32_t> node_colors =  my_dbg.get_node_colors(first_node);
            tot_colors+=node_colors.size();
            acc_fm_counts+=fm_count;
            //

            uint32_t seq_color;
            uint8_t symbol;
            bool is_ambigous;
            for(const auto& node_color: node_colors) {

                tmp_node = first_node;
                seq_color = node_color;

                bool seq_is_uncomplete = true;
                is_ambigous = false;
                std::string tmp_string;
                tmp_string = my_dbg.node2string(tmp_node);

                for(auto & sym : tmp_string) sym = dna_alphabet::char2comp[sym];

                while (seq_is_uncomplete) {

                    outd = my_dbg.outdegree(tmp_node);
                    if (outd == 1) {
                        symbol = (my_dbg.edge_bwt[my_dbg.get_edges(tmp_node).first]) >> 1U;
                        tmp_string.push_back(symbol);
                        tmp_node = my_dbg.outgoing(tmp_node, 1);
                    } else {

                        n_colors = 0;

                        for (size_t j = 1; j <= outd; j++) {
                            neighbor = my_dbg.outgoing(tmp_node, j);
                            for (auto const &color : my_dbg.get_node_colors(neighbor)) {
                                if (seq_color == color) {
                                    cand_node = neighbor;
                                    symbol = (my_dbg.edge_bwt[my_dbg.get_edges(tmp_node).first + j - 1]) >> 1U;
                                    n_colors++;
                                }
                            }
                        }

                        if (n_colors == 1) {
                            tmp_node = cand_node;
                            tmp_string.push_back(symbol);
                        } else {
                            /*for(auto &sym : tmp_string) sym = dna_alphabet::comp2char[sym];
                            tmp_string = tmp_string.substr(1, tmp_string.size()-1);
                            std::cout<<tmp_string<<std::endl;*/
                            is_ambigous = true;
                            seq_is_uncomplete = false;
                            n_ambigous++;
                        }
                    }

                    if (!my_dbg.solid_nodes[tmp_node]) {
                        seq_is_uncomplete = false;
                    }
                }

                if (!is_ambigous) {
                    tmp_string = tmp_string.substr(1, tmp_string.size()-2);

                    fm_count = sdsl::count(fm_index,tmp_string);
                    if(fm_count==0){
                        std::reverse(tmp_string.begin(), tmp_string.end());
                        for(auto& sym: tmp_string){
                            sym = dna_alphabet::comp2rev[sym];
                        }
                        fm_count = sdsl::count(fm_index,tmp_string);
                    }
                    ASSERT_NE(0, fm_count);
                }
            }
        }
    }

    std::cout<<n_ambigous<<std::endl;
    ASSERT_EQ(tot_colors, acc_fm_counts);
}

TEST_F(BOSSTests, outdegree){
}

TEST_F(BOSSTests, indegree){
}

TEST_F(BOSSTests, outgoing_rank){

}

TEST_F(BOSSTests, outgoing_symbol){

    gzFile fp;
    kseq_t *seq;
    int l;
    fp = gzopen(file.c_str(), "r");
    seq = kseq_init(fp);
    dbg_boss::size_type tmp_node=0, first_node=0;
    uint8_t symbol;

    while ((l = kseq_read(seq)) >= 0){

        for(size_t j=0;j<2;j++) {

            if(j==1){
                //compute the reverse complement of current read
                for(int i= 0, k = l-1; i < k; i++, k--){
                    symbol = seq->seq.s[i];
                    seq->seq.s[i] = dna_alphabet::char2rev[seq->seq.s[k]];
                    seq->seq.s[k] =  dna_alphabet::char2rev[symbol];
                }
                tmp_node = my_dbg.rev_comp(tmp_node);
            }else{
                tmp_node = my_dbg.string2node(seq->seq.s, my_dbg.k - 1);
                first_node = my_dbg.incomming(tmp_node,'$', true);
            }

            for (int i = my_dbg.k - 1; i <l; i++) {
                tmp_node = my_dbg.outgoing(tmp_node,
                                           seq->seq.s[i],
                                           true);
            }

            if(j==1){
                tmp_node = my_dbg.outgoing(tmp_node,'$', true);
                tmp_node = my_dbg.rev_comp(tmp_node);
            }
        }
        ASSERT_EQ(first_node, tmp_node);
    }
    kseq_destroy(seq);
    gzclose(fp);
}


TEST_F(BOSSTests, incomming_rank){

}

TEST_F(BOSSTests, incomming_symbol){

    gzFile fp;
    kseq_t *seq;
    int l;
    fp = gzopen(file.c_str(), "r");
    seq = kseq_init(fp);
    dbg_boss::size_type tmp_node=0, first_node=0;
    uint8_t symbol;

    while ((l = kseq_read(seq)) >= 0) {

        for(size_t j=0;j<2;j++) {

            if(j==1){//compute the reverse complement of current read
                for(int i= 0, k = l-1; i < k; i++, k--){
                    symbol = seq->seq.s[i];
                    seq->seq.s[i] = dna_alphabet::char2rev[seq->seq.s[k]];
                    seq->seq.s[k] =  dna_alphabet::char2rev[symbol];
                }
                tmp_node = my_dbg.rev_comp(tmp_node);
            }else{
                tmp_node = my_dbg.string2node(seq->seq.s+l-my_dbg.k+1, my_dbg.k - 1);
                first_node = my_dbg.outgoing(tmp_node, '$', true);
            }

            for (size_t i = l-my_dbg.k+1; i --> 0 ;){
                tmp_node = my_dbg.incomming(tmp_node,
                                           seq->seq.s[i],
                                           true);
            }

            if(j==1){
                tmp_node = my_dbg.incomming(tmp_node,'$', true);
                tmp_node = my_dbg.rev_comp(tmp_node);
            }
        }
        ASSERT_EQ(first_node, tmp_node);
    }
    kseq_destroy(seq);
    gzclose(fp);
}

TEST_F(BOSSTests, string2node){
    for(size_t i=0;i<my_dbg.solid_nodes.size();i++){
        if(my_dbg.solid_nodes[i]){
            ASSERT_EQ(i, my_dbg.string2node(my_dbg.node2string(i).c_str(), my_dbg.k - 1));
        }
    }
}

TEST_F(BOSSTests, backward_search){
    for(size_t i=0;i<my_dbg.solid_nodes.size();i++){
        if(my_dbg.solid_nodes[i]){
            std::vector<dbg_boss::bs_t> bs = my_dbg.backward_search(my_dbg.node2string(i),0);
            ASSERT_EQ(i, bs[0].r_start);
        }
    }
}

TEST_F(BOSSTests, reverse_complement){
    for(size_t i=0;i<my_dbg.solid_nodes.size();i++){
        if(my_dbg.solid_nodes[i]){
            ASSERT_EQ(i, my_dbg.rev_comp(my_dbg.rev_comp(i)));
        }
    }
}

int main(int argc, char **argv){
    ::testing::InitGoogleTest(&argc, argv);
    int ret = RUN_ALL_TESTS();
    return ret;
}

