#BOSS library

This C++ library is an implementation of BOSS; a BWT-based data structure used to succinctly represent a de Bruijn graph
from a set of DNA sequences. The original idea was described in [this](https://link.springer.com/chapter/10.1007/978-3-642-33122-0_18) article.

##Some notes about the implementation
* The input files are FASTA or FASTQ files
* Sequences with *N* symbols will be discarded
* We define a dBG node to be *solid* if its label is not padded with dollar/dummy symbols. We augmented BOSS with a
compressed bit vector that marks the positions where the solid nodes occur
in the matrix. Thus, we can move quickly between them. 
* The *outgoing* function cannot follow edges labeled with dollars/dummy. This feature is for practical reasons.  
* Our construction algorithm for BOSS needs to build the suffix array, the LCP array and the BWT of the sequences,
but computing these three data structures can difficult for huge files. However, we are working on a much
more efficient construction of the BWT+LCP that won't need the suffix array. We hope that our idea will significantly
reduce the time and space requirements for the construction of BOSS.
* For the moment, we consider the DNA reverse complement of every sequence for building the graph. This feature was
decided because our implementation is mainly intended to work with sequencing datasets. Turn it off is easy, but we haven't done it yet.

##Implemented functions (so far)

1. **outdegree(size_type node_id)**: number of outgoing edges of a node
2. **d_outdegree(size_type node_id)**: returns a tuple where the first element is the outdegree or the node and
the second element is a boolean indicating if one of the edges is labeled with dollar. 
3. **indegree(size_type node_id)**: number of incomming edges of a node 
4. **d_outdegree(size_type node_id)**: same idea as d_outdegree but with the incommings edges 
5. **outgoing(size_type, uint8_t value, bool is_symbol)**: follow the ith outgoing edge of node in lex order, where i=value.
If *is_symbol* is true, then the function follows the outgoing edge labeled with value. 
6. **incomming(size_type, uint8_t rank)**: follows the ith incomming edge in lex order, where i=rank. 
7. **string2node(std::string label)**: returns the id of the dBG node labeled with *label* 
8. **node2string(size_type node_id)**: returns the label of node *node_id* 
9. **backward_search(label_t query, size_t mismatches)**: returns the range of nodes in the BOSS matrix suffixed by *query*.
If *mismatches* is > 0, then this function returns a list of ranges where every range contains a set nodes suffixed by
some string s such that s has a edit distance of at least *mismatches* with *query*.  
10. **rev_comp(size_type node_id)**: returns the id whose label is the reverse complement of label of *node_id* 

##Requisites
1. CMake 3.7 or higher
2. SDSL-lite library

##Installation
First clone the project and enter to the root folder:
```bash
git clone https://DiegoDiazDominguez@bitbucket.org/DiegoDiazDominguez/boss.git
cd boss
```
Then prepare the files for compilation. If the SDSL library is in a default folder,
like your home or usr, then CMake will detect it automatically.
```bash
mkdir build && cd build
cmake ..
``` 
Finally, compile and install the library:
```bash
make & make install
```  
This procedure will install BOSS into the system directories. If you want a custom directory,
then you have to change this:
```bash
cmake .. -DCMAKE_INSTALL_PREFIX=/my/path/to/install/BOSS
``` 

##Examples
###Generating and storing the BOSS index
```C++
#include <boss.h>

int main(){
    std::string file="my_file.fastq"; //it can also be a fasta
    size_t kmer_size = 30;
    dbg_boss dbg_index(file, kmer_size);
    store_to_file(dbg_index, "my_dbg.boss");
    return 0;
}
```

###Loading the index from file
```
#include <boss.h>

int main(){
    dbg_boss dbg_index;
    load_from_file(dbg_index, "my_dbg.boss");
    return 0;
}
```
### Some functions
```
#include <boss.h>

int main(){
    dbg_boss dbg_index;
    load_from_file(dbg_index, "my_dbg.boss");
    dbg_boss::size_type tmp_solid;
    r=1;
    //iterate over solid nodes
    for(size_t i=1;i<=dbg_index.n_solid_nodes;i++){
        //select the position of the ith solid node
        tmp_solid = dbg_index.solid_nodes_ss(i);
        //get the indegree of the node
        std::cout<<dbg_index.indegree(tmp_solid)<<std::endl;
        //get its outdegree
        std::cout<<dbg_index.outdegree(tmp_solid)<<std::endl;
        //follow the outgoing edge labeled with 'A'
        std::cout<<dbg_index.outgoing(tmp_node, 'A', true)<<std::endl;;
        //follow the rth outgoing edge in lex order
        std::cout<<dbg_index.outgoing(tmp_node, r)<<std::endl;
        //get the label of the node
        std::cout<<dbg_index.node2string(tmp_node, r)<<std::endl;
    }
    return 0;
}
```


