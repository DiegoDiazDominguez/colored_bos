//
// Created by Diego Diaz on 2019-05-28.
//
#include "fastx_parser.hpp"
#include "boss.hpp"
#include "build_index.hpp"
#include <ctime>

int main(int argc, char* argv[]) {

    if (argc < 5) {
        std::cerr << "Usage: " << argv[0] << " INPUT_FASTQ_FILE INPUT_BOSS_INDEX N_TREADS OUTPUT_FILE" << std::endl;
        return 1;
    }

    sdsl::cache_config config;
    std::string input_file=argv[1];
    size_t n_threads= size_t(std::stoi(argv[3]));
    std::string output_prefix = argv[4];

    fastx_parser::preproc_reads(input_file,config,1);
    dbg_boss dbg_index;
    sdsl::load_from_file(dbg_index, argv[2]);

    std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
    dbg_boss::color_matrix_t cm = build_index::color_dbg(dbg_index, config, n_threads);
    std::chrono::steady_clock::time_point end= std::chrono::steady_clock::now();

    dbg_index.swap_color_matrix(cm);

    sdsl::store_to_file(dbg_index, output_prefix+".cboss");

    auto elapsed_time = std::chrono::duration_cast<std::chrono::seconds>(end - begin).count();

    std::ofstream output_file;
    output_file.open(output_prefix + ".index_stats");
    output_file<<"#kmer size: "<<dbg_index.k<<std::endl;
    output_file<<"#Number of dBG nodes: "<<dbg_index.tot_nodes()<<std::endl;
    output_file<<"#Number of dBG solid nodes: "<<dbg_index.n_solid_nodes<<std::endl;
    output_file<<"#Number of dBG edges: "<<dbg_index.tot_edges()<<std::endl;
    output_file<<"#Number of colored dBG nodes: "<< dbg_index.tot_colored_nodes()<<
               " ("<<(double(dbg_index.tot_colored_nodes())/dbg_index.tot_nodes())*100<<"% of the dBG nodes)"<<std::endl;
    output_file<<"#Number of different colors: "<< dbg_index.tot_colors()<<std::endl;
    output_file<<"#Elapsed time (seconds): "<< elapsed_time<<std::endl;
    output_file<<"#Size in MB of colored dBG index: "<<sdsl::size_in_mega_bytes(dbg_index)<<std::endl;
    output_file<<"#Percentage of the index space used by the colors: "<< dbg_index.color_space_frac()*100<<std::endl;
    output_file<<"#Frequency of every color"<<std::endl;
    output_file<<"#Color\tfreq"<<std::endl;

    std::pair<std::map<size_t, size_t>,
            std::map<size_t, size_t>> color_stats = dbg_index.color_stats();
    for(auto const& item : color_stats.first){
        output_file<<item.first<<"\t"<<item.second<<std::endl;
    }
    output_file<<"#Frencency of colored nodes"<<std::endl;
    output_file<<"#Number_of_colors_in_node\tfreq"<<std::endl;
    for(auto const& item : color_stats.second){
        output_file<<item.first<<"\t"<<item.second<<std::endl;
    }
    output_file.close();
    sdsl::util::delete_all_files(config.file_map);
}
