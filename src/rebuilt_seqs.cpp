//
// Created by Diego Diaz on 2019-05-09.
//

#include "dna_alphabet.hpp"
#include "boss.hpp"
#include <fstream>
#include <thread>
#include <mutex>
#include <sdsl/csa_wt.hpp>
#include <sdsl/suffix_arrays.hpp>

std::mutex mtx_seqs;

void compute_starters(size_t start,
                      size_t end,
                      dbg_boss& my_dbg,
                      std::unordered_map<dbg_boss::size_type, bool>& starter_hash){

    dbg_boss::size_type prev_node;

    //TODO just testing
    //size_t tot_starter_colors=0;
    //size_t tot_starter_colors=0, n_colors, fm_count;
    //sdsl::csa_wt<wt_huff<rrr_vector<127> >, 512, 1024> fm_index;
    //std::string fm_index_file = "/home/ddiaz/colored_boss/ecoli_15x.fm_index";
    //sdsl::load_from_file(fm_index, fm_index_file);
    //

    for(size_t i=start;i<=end;i++) {
        prev_node = my_dbg.incomming(my_dbg.solid_nodes_ss(i), '$', true);
        if(prev_node!=0){
            try {
                std::lock_guard<std::mutex> lck(mtx_seqs);
                if (starter_hash.count(prev_node) == 0) {
                    starter_hash[prev_node] = true;
                    //tot_starter_colors+= my_dbg.get_node_colors(prev_node).size();
                }
            }catch (std::logic_error&){
                std::cout<<"some error in the threads"<<std::endl;
            }

            //TODO testing
            //checking that the number of colors is ok
            /*std::string kmer_seq = my_dbg.node2string(prev_node);
            for(auto & sym : kmer_seq) sym = dna_alphabet::char2comp[sym];
            fm_count = sdsl::count(fm_index,kmer_seq);

            std::reverse(kmer_seq.begin(), kmer_seq.end());
            for(auto& sym: kmer_seq)sym = dna_alphabet::comp2rev[sym];
            fm_count += sdsl::count(fm_index,kmer_seq);
            if(fm_count!=n_colors){
                std::cout<<"whut?"<<n_colors<<" "<<fm_count<<" "<<my_dbg.node2string(prev_node);
            }
            tot_starter_colors+=n_colors;*/
            //
        }
    }

    //TODO testing
    //std::cout<<"There are "<<tot_starter_colors<<" colors in starters"<<std::endl;
}

void build_sequences(size_t start,
                     size_t end,
                     size_t& seqs_rebuilt,
                     size_t& amb_seqs,
                     std::vector<dbg_boss::size_type>& starter_nodes,
                     dbg_boss& my_dbg,
                     sdsl::csa_wt<wt_huff<rrr_vector<127> >, 512, 1024>& fm_index,
                     std::ofstream& output_file){

    dbg_boss::size_type first_node, tmp_node, neighbor, cand_node;
    size_t seq_color, outd, n_colors, n_ambigous=0, seq_counter=0;
    bool ambigous;
    uint8_t symbol;
    std::stringstream ss;


    size_t tmp=0;
    for(size_t i=start;i<=end;i++){

        first_node = starter_nodes[i];

        if(first_node!=0){//tmp_node is the start of one or more sequences
            auto colors =  my_dbg.get_node_colors(first_node);
            tmp+=colors.size();

            for(const auto& node_color: colors) {

                tmp_node = first_node;
                seq_color = node_color;
                bool flag = true;
                ambigous = false;
                std::string tmp_string;
                tmp_string = my_dbg.node2string(tmp_node);
                for(auto & sym : tmp_string) sym = dna_alphabet::char2comp[sym];

                while (flag) {

                    outd = my_dbg.outdegree(tmp_node);
                    if (outd == 1) {
                        symbol = my_dbg.edge_bwt[my_dbg.get_edges(tmp_node).first] >> 1U;
                        tmp_string.push_back(symbol);
                        tmp_node = my_dbg.outgoing(tmp_node, 1);
                    } else {

                        n_colors = 0;

                        for (size_t j = 1; j <= outd; j++) {
                            neighbor = my_dbg.outgoing(tmp_node, j);
                            for (auto const &color : my_dbg.get_node_colors(neighbor)) {
                                if (seq_color == color) {
                                    cand_node = neighbor;
                                    symbol = my_dbg.edge_bwt[my_dbg.get_edges(tmp_node).first + j - 1] >> 1U;
                                    n_colors++;
                                }
                            }
                        }

                        if (n_colors == 1) {
                            tmp_node = cand_node;
                            tmp_string.push_back(symbol);
                        } else {
                            //std::cout<<my_dbg.node2string(tmp_node)<<std::endl;
                            ambigous = true;
                            flag = false;
                            n_ambigous++;
                        }

                    }

                    if (!my_dbg.solid_nodes[tmp_node]) {
                        flag = false;
                    }
                }

                if (!ambigous) {

                    tmp_string = tmp_string.substr(1, tmp_string.size()-2);
                    seq_counter++;

                    size_t fm_count = sdsl::count(fm_index,tmp_string);
                    if(fm_count==0){
                        std::reverse(tmp_string.begin(), tmp_string.end());
                        for(auto& sym: tmp_string){
                            sym = dna_alphabet::comp2rev[sym];
                        }
                        fm_count = sdsl::count(fm_index,tmp_string);
                    }

                    if(fm_count==0){
                        for(auto&sym : tmp_string) sym = dna_alphabet::comp2char[sym];
                        std::cout<<tmp_string<<std::endl;
                    }
                    assert(fm_count!=0);
                }/*else{
                    for(auto&sym : tmp_string) sym = dna_alphabet::comp2char[sym];
                    std::cout<<"soy amb: "<<tmp_string<<std::endl;
                }*/
            }
        }
    }

    /*if(printed_syms!=0){
        try {
            std::lock_guard<std::mutex> lck(mtx_seqs);
            output_file << ss.str();
        }catch (std::logic_error&){
            std::cout<<"some error in the threads"<<std::endl;
        }
        ss.clear();
    }*/

    try {
        std::lock_guard<std::mutex> lck(mtx_seqs);
        seqs_rebuilt+=seq_counter;
        amb_seqs +=n_ambigous;
    }catch (std::logic_error&){
        std::cout<<"some error in the threads"<<std::endl;
    }
}

int main(int argc, char* argv[]) {

    if (argc < 5) {
        std::cerr << "Usage: " << argv[0] << " INPUT_BOSS_INDEX INPUT_FM_INDEX N_TREADS OUTPUT_FILE" << std::endl;
        return 1;
    }

    dbg_boss dbg_index;
    sdsl::load_from_file(dbg_index, argv[1]);

    sdsl::csa_wt<wt_huff<rrr_vector<127> >, 512, 1024> fm_index;
    sdsl::load_from_file(fm_index, argv[2]);

    size_t n_threads = size_t(std::stoi(argv[3]));
    std::ofstream output_file;

    output_file.open(std::string(argv[4]) + ".fasta");

    std::vector<dbg_boss::size_type> starter_nodes;

    dbg_boss::size_type start, end;
    size_t rem = dbg_index.n_solid_nodes % n_threads;

    std::cout<<"computing the start of every sequence"<<std::endl;
    {
        dbg_boss::size_type nodes_per_thread = dbg_index.n_solid_nodes / n_threads;
        std::vector<std::thread> threads;
        std::unordered_map<dbg_boss::size_type, bool> starters_hash;
        for (size_t i = 0; i < n_threads; i++) {
            start = i * nodes_per_thread + 1;
            end = ((i + 1) * nodes_per_thread);
            if (i == n_threads - 1) end += rem;

            threads.emplace_back(std::thread(compute_starters,
                                             start,
                                             end,
                                             std::ref(dbg_index),
                                             std::ref(starters_hash)));

        }
        for (auto &it : threads) it.join();

        starter_nodes.reserve(starters_hash.size());
        for (auto const &imap: starters_hash) starter_nodes.push_back(imap.first);
    }

    std::vector<std::thread> threads;
    dbg_boss::size_type seqs_per_thread = starter_nodes.size() / n_threads;
    rem = starter_nodes.size() % n_threads;
    size_t amb_seqs=0, reb_seqs=0;

    std::cout<<"inferring the original sequences"<<std::endl;
    for (size_t i = 0; i < n_threads; i++) {

        start = i * seqs_per_thread;
        end = ((i + 1) * seqs_per_thread)-1;
        if (i == n_threads - 1) end += rem;
        threads.emplace_back(std::thread(build_sequences,
                                         start,
                                         end,
                                         std::ref(reb_seqs),
                                         std::ref(amb_seqs),
                                         std::ref(starter_nodes),
                                         std::ref(dbg_index),
                                         std::ref(fm_index),
                                         std::ref(output_file)));
    }
    for (auto &it : threads) it.join();
    output_file.close();
    std::cout<<reb_seqs<<" were rebuilt and "<<amb_seqs<<" were ambiguous"<<std::endl;
}

