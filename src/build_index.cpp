//
// Created by Diego Diaz on 2019-04-10.
//

#include "build_index.hpp"
#include <bitset>
#include <sdsl/construct.hpp>
#include <ctime>

std::mutex mtx;

void build_index::build_SA_BWT_LCP(sdsl::cache_config &config) {
    // construct SA
    sdsl::construct_sa<8>(config);
    register_cache_file(sdsl::conf::KEY_SA, config);

    // construct BWT
    sdsl::construct_bwt<8>(config);
    register_cache_file(sdsl::conf::KEY_BWT, config);

    // construct LCP
    sdsl::construct_lcp_semi_extern_PHI(config);
    register_cache_file(sdsl::conf::KEY_LCP, config);
}

void build_index::build_edge_BWT(sdsl::cache_config &config, size_t K) {

    bool flag_symbols;
    std::bitset<6> kmer_symbols;
    std::bitset<6> flagged_symbols=false;
    sdsl::bit_vector node_marks;
    sdsl::int_vector_buffer<8> bwt(cache_file_name(sdsl::conf::KEY_BWT, config));
    sdsl::int_vector_buffer<8> ebwt(cache_file_name("ebwt", config), std::ios::out);
    sdsl::int_vector_buffer<> klcp(cache_file_name("klcp", config));
    size_t bwt_pos=0;

    //this is for marking dollar-prefixed kmers
    sdsl::sd_vector<> dollar_bv;
    sdsl::bit_vector solid_marks;
    load_from_file(dollar_bv, cache_file_name("sp_dollar_bv", config));
    sdsl::sd_vector<>::rank_1_type dr(&dollar_bv);
    sdsl::int_vector_buffer<> sa(cache_file_name(sdsl::conf::KEY_SA, config));
    size_t node_pos=0;

    //
    size_t n_dollars = dr(dollar_bv.size());

    //TODO just testing
    //int_vector_buffer<8> text(cache_file_name(conf::KEY_TEXT, config));
    //

    sdsl::util::assign(node_marks, sdsl::int_vector<1>(bwt.size(), 1));
    sdsl::util::assign(solid_marks, sdsl::int_vector<1>(bwt.size(), 0));

    //TODO just testing
    /*for(size_t i=0;i<20;i++){
        std::cout<<klcp[i]<<" ";
        for(size_t j=sa[i];j<std::min(sa[i]+50, text.size());j++){
            std::cout<<dna_alphabet::comp2char[text[j]];
        }
        std::cout<<""<<std::endl;
    }*/
    //

    //set the dummy node
    kmer_symbols[1]=true;
    for(size_t i=0;i<n_dollars;i++){
        if(!kmer_symbols[bwt[i]]) kmer_symbols[bwt[i]]=true;
        //62=111110
        if(kmer_symbols==62)break;
    }

    for(size_t j=1;j<kmer_symbols.size();j++) {
        if (kmer_symbols[j]) {
            ebwt[bwt_pos] = (j << 1U);
            bwt_pos++;
        }
    }
    node_marks[bwt_pos-1] = false;
    node_pos++;
    kmer_symbols.reset();
    //

    for(size_t i=2;i<=klcp.size();i++){

        //TODO just checking
        //bool first=true;

        if((i==klcp.size() || klcp[i]<K) && kmer_symbols!=0){

            //this is for marking solid nodes
            if(dr.rank(std::min(sa[i-1]+K, sa.size()))-dr.rank(sa[i-1])==0){
                solid_marks[node_pos]=true;
            }


            if(i<=(n_dollars+1)){

                ebwt[bwt_pos] = 3;
                bwt_pos++;

                //TODO just testing
                /*if(color_marks[node_pos]) {
                    std::cout << epos << "\t" << (ebwt[bwt_pos - 1] & 1UL)
                              << (dna_alphabet::comp2char[ebwt[bwt_pos - 1] >> 1U]) << "\t"
                              << i << "\t";

                    epos++;
                    bool dollar_start = false;
                    for (size_t i1 = 0; i1 < K; i1++) {
                        if (((sa[i - 1] + i1) >= text.size() || (text[sa[i - 1] + i1]) == 1) && text[sa[i - 1]] != 1) {
                            dollar_start = true;
                        }

                        if (!dollar_start) {
                            std::cout << dna_alphabet::comp2char[text[sa[i - 1] + i1]];
                        } else {
                            std::cout << "$";
                        }
                    }
                    std::cout << "\t" << solid_marks[node_pos] << "\t" << color_marks[node_pos]<<"\t"<<node_pos;
                    kpos++;
                    std::cout << "" << std::endl;
                }*/
                //

            }else {
                //store the symbols of the previous kmer
                for(size_t j=1;j<kmer_symbols.size();j++){

                    if(kmer_symbols[j]){
                        ebwt[bwt_pos] = (j << 1U) | flagged_symbols[j];
                        bwt_pos++;

                        //TODO just checking
                        /*if((node_pos>=50810 && node_pos<50820) || node_pos==18788) {
                            std::cout << bwt_pos-1 << "\t" << (ebwt[bwt_pos - 1] & 1UL)
                                      << (dna_alphabet::comp2char[ebwt[bwt_pos - 1] >> 1U]) << "\t"
                                      << i << "\t";
                            epos++;
                            if (first) {
                                bool dollar_start = false;
                                for (size_t i1 = 0; i1 < K; i1++) {

                                    if (((sa[i - 1] + i1) >= text.size() || (text[sa[i - 1] + i1]) == 1) &&
                                        text[sa[i - 1]] != 1) {
                                        dollar_start = true;
                                    }

                                    if (!dollar_start) {
                                        std::cout << dna_alphabet::comp2char[text[sa[i - 1] + i1]];
                                    } else {
                                        std::cout << "$";
                                    }
                                }
                                std::cout << "\t" << solid_marks[node_pos]<<"\t"<<color_marks[node_pos]<<"\t"<<node_pos;
                                kpos++;
                            }
                            first = false;
                            std::cout << "" << std::endl;
                        }*/
                        //
                    }
                }
            }

            node_marks[bwt_pos-1] = false;
            node_pos++;
            if(i==klcp.size()) break;

            //setup for the next Kmer;
            flag_symbols = klcp[i] == (K - 1);
            if (!flag_symbols) {
                flagged_symbols.reset();
            } else {
                flagged_symbols |= kmer_symbols;
            }
            kmer_symbols.reset();
        }
        kmer_symbols.set(bwt[i]);
    }

    node_marks.resize(bwt_pos);
    solid_marks.resize(node_pos);

    ebwt.close();
    klcp.close();

    sdsl::wt_huff<> tmp_wt;
    sdsl::construct(tmp_wt, cache_file_name("ebwt", config));

    sdsl::int_vector<64> tmp_C;
    tmp_C.resize(dna_alphabet::sigma+1);

    //compute the rank of every symbol
    tmp_C[0]=0;// # symbol
    for(uint8_t i=1;i<dna_alphabet::sigma;i++) tmp_C[i] = tmp_wt.rank(tmp_wt.size(),(i<<1U));

    uint64_t tmp1, tmp2;
    tmp2 = 0;
    for(uint8_t i=1;i<=dna_alphabet::sigma;i++){
        tmp1 = tmp_C[i];
        tmp_C[i] = tmp_C[i-1] + tmp2;
        tmp2 = tmp1;
    }

    /*size_t tmp_cont=0;
    for(auto && color_mark : color_marks){
        if(color_mark){
            tmp_cont++;
        }
    }
    std::cout<<"estimated what?:"<<tmp_cont<<std::endl;*/

    //wavelet three with the edges
    store_to_file(tmp_wt, cache_file_name("ebwt_wt_huff", config));

    //first column (F array in the FM-index)
    f_array_t tmp_alph(tmp_C);
    store_to_file(tmp_alph, cache_file_name("f_array", config));

    //bit vector marking solid nodes
    sdsl::rrr_vector<63> rrr_solid_marks(solid_marks);
    store_to_file(rrr_solid_marks, cache_file_name("solid_nodes", config));

    //bit vector marking edges
    sdsl::rrr_vector<63> tmp_node_marks(node_marks);
    store_to_file(tmp_node_marks, cache_file_name("node_marks", config));

    //remove unnecessary files
    remove(cache_file_name(sdsl::conf::KEY_SA, config));
    remove(cache_file_name(sdsl::conf::KEY_BWT, config));
    remove(cache_file_name("ebwt", config));
    remove(cache_file_name("klcp", config));
    remove(cache_file_name("sp_dollar_bv", config));

    register_cache_file("solid_nodes", config);
    register_cache_file("node_marks", config);
    register_cache_file("ebwt_wt_huff", config);
    register_cache_file("f_array", config);
}

void build_index::build_KLCP(sdsl::cache_config &config, size_t K) {

    size_t n_dollars;
    sdsl::int_vector_buffer<> lcp(cache_file_name(sdsl::conf::KEY_LCP, config));
    sdsl::int_vector_buffer<> klcp(cache_file_name("klcp", config), std::ios::out, 1000000);

    sdsl::int_vector_buffer<> sa(cache_file_name(sdsl::conf::KEY_SA, config));
    sdsl::sd_vector<> dollar_bv;
    load_from_file(dollar_bv, cache_file_name("sp_dollar_bv", config));
    sdsl::sd_vector<>::rank_1_type dr(&dollar_bv);
    sdsl::sd_vector<>::select_1_type ds(&dollar_bv);
    n_dollars = dr.rank(dollar_bv.size());

    klcp[0] = 0;

    //dummy node
    //TODO K cannot be greater than the smallest read!
    for(size_t i=1;i<=n_dollars;i++){
        if(lcp[i]>K){
            klcp[i] = K;
        }else{
            klcp[i] = lcp[i];
        }
    }

    for(size_t i=(n_dollars+1);i<lcp.size();i++){
        //modify the LCP
        if(lcp[i]>K || lcp[i]>=(ds.select(dr.rank(sa[i]+1)+1)-sa[i]+1)){
            klcp[i]=K;
        }else{
            klcp[i] = lcp[i];
        }
    }

    lcp.close();
    klcp.close();

    remove(cache_file_name(sdsl::conf::KEY_LCP, config));
}

void build_index::build_boss(std::string input_file, sdsl::cache_config &config, size_t K) {
    fastx_parser::preproc_reads(input_file, config, 1);
    build_SA_BWT_LCP(config);
    build_KLCP(config, K - 1);
    build_edge_BWT(config, K - 1);
}

build_index::color_matrix_t build_index::color_dbg(dbg_boss &dbg_index,
                                                   cache_config &config,
                                                   size_t n_threads) {

    size_t elm_per_thread, rem, start, end;

    matrix_skeleton m_skeleton;

    std::cout<<"Computing the skeleton of the color matrix"<<std::endl;
    build_matrix_skeleton(dbg_index, m_skeleton, n_threads, config);

    //coloring the nodes
    std::cout<<"Filling the matrix"<<std::endl;
    std::vector<std::thread> threads;
    text_t text(cache_file_name(conf::KEY_TEXT, config));
    elm_per_thread = text.size()/n_threads;
    rem = text.size() % n_threads;
    text.close();

    for(size_t i=0;i<n_threads;i++){
        start = i*elm_per_thread;
        end = (i+1)*elm_per_thread;
        if(i==n_threads-1) end +=rem-1;
        threads.emplace_back(std::thread(color_dbg_internal,
                                         std::ref(dbg_index),
                                         start,end,
                                         std::ref(m_skeleton),
                                         std::ref(config)));
    }
    for (auto & it : threads) it.join();
    //

    //store the compressed version of the color matrix
    std::cout<<"Compacting the matrix"<<std::endl;
    size_type n_rows = m_skeleton.colored_rows_rs(m_skeleton.colored_rows.size());
    for(size_type j=1;j<=n_rows;j++){

        start = m_skeleton.colored_cells_ss(j);
        end = m_skeleton.colored_cells_ss(j+1)-1;

        std::vector<uint16_t> node_colors(end-start+1);
        for(size_t i=start,k=0;i<=end;i++,k++) {
            node_colors[k] = m_skeleton.cell_values[i];
            assert(node_colors[k]!=0);
        }

        std::sort(node_colors.begin(), node_colors.end());

        for(size_t i=start,k=0;i<=end;i++,k++){
            if(i==start){
                m_skeleton.cell_values[i] = node_colors[k];
            }else{
                m_skeleton.cell_values[i] = node_colors[k]-node_colors[k-1];
                assert((node_colors[k]-node_colors[k-1])!=0);
            }
        }
    }
    color_matrix_t cm(m_skeleton);
    return cm;
}

void build_index::color_dbg_internal(dbg_boss &dbg_index,
                                     long long int start,
                                     long long int end,
                                     matrix_skeleton& m_skeleton,
                                     sdsl::cache_config &config) {

    text_t text(cache_file_name(conf::KEY_TEXT, config));
    size_t outd, ind;
    size_type tmp_node, neighbor_node, neighbor_node_2, node_row, row_pos, last_row_pos, tmp1, tmp2, middle;

    dbg_boss::label_t label(dbg_index.k-1);
    std::map<dbg_boss::size_type, bool> to_be_colored; //nodes to be colored in the current read
    std::map<dbg_boss::size_type, bool> to_be_inspected; //neighboring nodes of the current read from which inspect get_colors

    sdsl::bit_vector used_colors_bv;

    std::vector<uint8_t> syms(dna_alphabet::sigma*2);
    std::vector<size_type> r_b(dna_alphabet::sigma*2);
    std::vector<size_type> r_a(dna_alphabet::sigma*2);

    //I am using a buffer which is big enough
    std::vector<std::pair<size_type, size_type>> insp_ranges(10000);
    std::vector<std::pair<size_type, size_type>> col_ranges(10000);

    uint32_t new_color;
    long long int text_pos;

    //PARALLEL
    //adjust start and end
    while(text[start]!=1) start--;
    while(text[end]!=1) end--;

    text_pos=end;
    while(text_pos>start){

        for(size_t j=dbg_index.k-1;j-->0;){
            label[j] = text[text_pos];
            text_pos--;
        }

        //the node labelled with the start of the read
        tmp_node = dbg_index.backward_search(label,
                                             0, syms,
                                             r_b, r_a)[0].r_start;

        assert(tmp_node!=0 && m_skeleton.colored_rows[tmp_node]);
        node_row = m_skeleton.colored_rows_rs(tmp_node);

        //the starting dBG node of the read
        to_be_colored[node_row] = true;
        to_be_inspected[node_row] = true;

        outd = dbg_index.outdegree(tmp_node);
        if(outd>1){
            for(size_t i=1;i<=outd;i++){
                neighbor_node = dbg_index.outgoing(tmp_node, i, false);
                to_be_inspected[m_skeleton.colored_rows_rs(neighbor_node)]=true;
                assert(m_skeleton.colored_rows[neighbor_node]);
            }
        }
        //

        while(text[text_pos+1]!=1){
            tmp_node = dbg_index.outgoing(tmp_node,
                                          text[text_pos],
                                          true, syms, r_b, r_a);


            if(m_skeleton.colored_rows[tmp_node]){

                node_row = m_skeleton.colored_rows_rs(tmp_node);
                to_be_colored[node_row] = true;

                // This is not strictly necessary.
                // However, it means that if two reads
                // have the same suffix, then they might
                // have the same color. Again, this is not a problem, but it requires to check the colors of the nodes,
                // to avoid repeating colors, and that can be more expensive in time.
                if(!dbg_index.solid_nodes[tmp_node]){
                    to_be_inspected[node_row] = true;
                    assert(m_skeleton.colored_rows[tmp_node]);
                }
            }

            outd = dbg_index.outdegree(tmp_node);
            if(outd>1){
                for(size_t i=1;i<=outd;i++){
                    neighbor_node = dbg_index.outgoing(tmp_node, i, false);
                    to_be_inspected[m_skeleton.colored_rows_rs(neighbor_node)]=true;
                    assert(m_skeleton.colored_rows[neighbor_node]);
                }
            }

            //this is necessary to cover more complex graph topologies
            ind = dbg_index.indegree(tmp_node);
            if(ind>1){
                for(size_t i=1;i<=ind;i++){

                    neighbor_node = dbg_index.incomming(tmp_node, i, false);

                    if(m_skeleton.colored_rows[neighbor_node]){
                        to_be_inspected[m_skeleton.colored_rows_rs(neighbor_node)]=true;
                    }

                    outd = dbg_index.outdegree(neighbor_node);
                    if(outd>1){
                        for(size_t j=1;j<=outd;j++){
                            neighbor_node_2 = dbg_index.outgoing(neighbor_node, j, false);
                            to_be_inspected[m_skeleton.colored_rows_rs(neighbor_node_2)]=true;
                            assert(m_skeleton.colored_rows[neighbor_node_2]);
                        }
                    }
                }
            }
            text_pos--;
        }

        sdsl::util::assign(used_colors_bv, sdsl::int_vector<1>(65535, 0));
        size_t cont=0;
        assert(to_be_inspected.size()<10000);

        // make a preliminary inspection of
        // the colors that are already used
        for (auto const &node: to_be_inspected) {

            row_pos = m_skeleton.colored_cells_ss(node.first+1),
            last_row_pos = m_skeleton.colored_cells_ss(node.first+2)-1;

            uint16_t color;
            while(row_pos <= last_row_pos){
                color = m_skeleton.cell_values[row_pos];
                if(color==0)break;
                used_colors_bv[color] = true;
                row_pos++;
            }

            insp_ranges[cont] = {row_pos, last_row_pos};
            cont++;
        }

        cont=0;
        for (auto const &node: to_be_colored) {
            col_ranges[cont] = {m_skeleton.colored_cells_ss(node.first+1),
                                m_skeleton.colored_cells_ss(node.first+2)-1};
            cont++;
        }
        //END PARALLEL


        //SEQUENTIAL
        try {
            //retrieve the get_colors that we cannot use for the current read
            std::lock_guard<std::mutex> lck(mtx);

            //find all used colors
            for (size_t i=0;i<to_be_inspected.size();i++) {

                row_pos = insp_ranges[i].first;
                last_row_pos = insp_ranges[i].second;

                uint16_t color;
                while(row_pos <= last_row_pos){
                    color = m_skeleton.cell_values[row_pos];
                    if(color==0)break;
                    used_colors_bv[color] = true;
                    row_pos++;
                }
            }

            //find the next available color
            new_color = 1;
            while(used_colors_bv[new_color]) new_color++;
            assert((32-__builtin_clzl(new_color))<=16);

            //assign the new color to the nodes that need to be colored
            for (size_t i=0;i<to_be_colored.size();i++) {

                row_pos = col_ranges[i].first;
                last_row_pos = col_ranges[i].second;


                //binary search the next available position
                tmp1 = row_pos;
                tmp2 = last_row_pos;
                while (tmp1 <= tmp2) {
                    middle = tmp1 + (tmp2 - tmp1) / 2;

                    // Check if x is present at mid
                    if (m_skeleton.cell_values[middle] == 0 &&
                         (middle==tmp1 || m_skeleton.cell_values[middle-1]!=0 )){
                        row_pos=middle;
                        break;
                    }

                    // If x greater, ignore left half
                    if (m_skeleton.cell_values[middle] != 0){
                        tmp1 = middle + 1;
                    }else{
                        tmp2 = middle - 1;
                    }
                }
                assert(row_pos<=last_row_pos);
                m_skeleton.cell_values[row_pos]=new_color;
                //
            }
            //END SEQUENTIAL
        }catch (std::logic_error&){
            std::cout<<"some error in the threads"<<std::endl;
        }

        to_be_colored.clear();
        to_be_inspected.clear();
        text_pos++;
    }
}

void build_index::estimate_color_rows(dbg_boss &dbg_index,
                                      size_type start,
                                      size_type end,
                                      sdsl::bit_vector &color_marks) {

    size_t ind;

    for(size_type i=start; i<=end;i++){

        if(i==0) continue;

        if(!dbg_index.solid_nodes[i]){
            if(dbg_index.solid_nodes[dbg_index.incomming(i,1)]){
                color_marks[i] = true;
            }else if(dbg_index.solid_nodes[dbg_index.outgoing(i,1)]){
                color_marks[i] = true;
            }
        }else{
            ind = dbg_index.indegree(i);
            for(size_t j=1;j<=ind;j++){
                if(dbg_index.outdegree(dbg_index.incomming(i,j))>1){
                    color_marks[i] = true;
                }
            }
        }
    }
}

void build_index::estimate_color_cells(dbg_boss &dbg_index,
                                       long long int start,
                                       long long int end,
                                       sdsl::cache_config &config,
                                       sdsl::rrr_vector<63> &color_marks,
                                       sdsl::rrr_vector<63>::rank_1_type &color_marks_rs,
                                       std::map<build_index::size_type, uint32_t> &c_nodes) {

    text_t text(cache_file_name(conf::KEY_TEXT, config));

    //adjust start and end
    while(text[start]!=1) start--;
    while(text[end]!=1) end--;
    long long int text_pos;

    //temporal files
    dbg_boss::label_t label(dbg_index.k-1);
    std::vector<uint8_t> syms(dna_alphabet::sigma*2);
    std::vector<size_type> r_b(dna_alphabet::sigma*2);
    std::vector<size_type> r_a(dna_alphabet::sigma*2);
    size_type tmp_node;

    std::map<size_type, bool> kmers_in_read;
    text_pos=end;
    while(text_pos>start) {

        for(size_t j=dbg_index.k-1;j-->0;){
            label[j] = text[text_pos];
            text_pos--;
        }
        tmp_node = dbg_index.backward_search(label,
                                             0, syms,
                                             r_b, r_a)[0].r_start;

        kmers_in_read[color_marks_rs(tmp_node)]=true;

        while(text[text_pos+1]!=1) {

            tmp_node = dbg_index.outgoing(tmp_node,
                                          text[text_pos],
                                          true, syms, r_b, r_a);
            if(color_marks[tmp_node]){
                kmers_in_read[color_marks_rs(tmp_node)]=true;
            }

            text_pos--;
        }

        for(auto const &kmer : kmers_in_read){
            c_nodes[kmer.first]++;
        }

        kmers_in_read.clear();
        text_pos++;
    }
}

void build_index::build_matrix_skeleton(dbg_boss &dbg_index,
                                        matrix_skeleton &skeleton,
                                        size_t n_threads,
                                        sdsl::cache_config &config) {


    size_t elm_per_thread, rem, start, end;

    //marking the nodes that need to be colored can be done in parallel
    elm_per_thread = dbg_index.tot_nodes()/n_threads;
    rem = dbg_index.tot_nodes() % n_threads;

    {
        bit_vector colored_rows_bv;
        sdsl::util::assign(colored_rows_bv, sdsl::int_vector<1>(dbg_index.tot_nodes(), 0));
        std::vector<std::thread> col_marks_threads;
        for (size_t i = 0; i < n_threads; i++) {
            start = i * elm_per_thread;
            end = (i + 1) * elm_per_thread - 1;
            if (i == n_threads - 1) end += rem;
            col_marks_threads.emplace_back(std::thread(estimate_color_rows,
                                                       std::ref(dbg_index),
                                                       start,
                                                       end,
                                                       std::ref(colored_rows_bv)));
        }
        for (auto &it : col_marks_threads) it.join();
        sdsl::rrr_vector<63> colored_rows(colored_rows_bv);
        skeleton.colored_rows.swap(colored_rows);
    }
    skeleton.colored_rows_rs.set_vector(&skeleton.colored_rows);


    {
        text_t text(cache_file_name(conf::KEY_TEXT, config));
        elm_per_thread = text.size() / n_threads;
        rem = text.size() % n_threads;
        text.close();

        std::map<size_type, uint32_t> all_color_map;
        size_type tot_color_cells = 0;
        {
            //building the array that will contain the uncompressed colors
            std::vector<std::thread> col_est_threads;
            std::vector<std::map<size_type, uint32_t>> color_maps(n_threads);
            for (size_t i = 0; i < n_threads; i++) {
                start = i * elm_per_thread;
                end = (i + 1) * elm_per_thread;
                if (i == n_threads - 1) end += rem - 1;
                col_est_threads.emplace_back(std::thread(estimate_color_cells,
                                                         std::ref(dbg_index),
                                                         start, end,
                                                         std::ref(config),
                                                         std::ref(skeleton.colored_rows),
                                                         std::ref(skeleton.colored_rows_rs),
                                                         std::ref(color_maps[i])));
            }
            for (auto &it : col_est_threads) it.join();

            //count the number of colors
            for (size_t i = 0; i < n_threads; i++) {
                for (auto const &node: color_maps[i]) {
                    all_color_map[node.first] += node.second;
                    tot_color_cells += node.second;
                }
            }

            //TODO testing
            /*std::vector<std::thread> amb_threads;
            size_t n_amb=0;
            for (size_t i = 0; i < n_threads; i++) {
                start = i * elm_per_thread;
                end = (i + 1) * elm_per_thread;
                if (i == n_threads - 1) end += rem - 1;
                amb_threads.emplace_back(std::thread(estimate_amb_seqs,
                                                     std::ref(dbg_index),
                                                     start, end,
                                                     std::ref(config),
                                                     std::ref(skeleton.colored_rows),
                                                     std::ref(skeleton.colored_rows_rs),
                                                     std::ref(n_amb)));
            }
            for (auto &it : amb_threads) it.join();
            std::cout<<"there are "<<n_amb<<" ambiguous reads"<<std::endl;*/
            //
        }

        bit_vector colored_cells;
        sdsl::util::assign(colored_cells, sdsl::int_vector<1>(tot_color_cells + 1, 0));

        size_type pos = 0;
        colored_cells[pos] = true;

        for (auto const &node: all_color_map) {
            pos += node.second;
            colored_cells[pos] = true;
        }

        sdsl::rrr_vector<63> comp_colored_cells(colored_cells);
        skeleton.colored_cells.swap(comp_colored_cells);
        skeleton.colored_cells_ss.set_vector(&skeleton.colored_cells);
        //
    }
    sdsl::util::assign(skeleton.cell_values, sdsl::int_vector<16>(skeleton.colored_cells.size()-1, 0));
}

void
build_index::estimate_amb_seqs(dbg_boss &dbg_index, long long int start, long long int end, sdsl::cache_config &config,
                               sdsl::rrr_vector<63> &color_marks, sdsl::rrr_vector<63>::rank_1_type &color_marks_rs,
                               size_t& tot_amb) {

    text_t text(cache_file_name(conf::KEY_TEXT, config));

    //adjust start and end
    while(text[start]!=1) start--;
    while(text[end]!=1) end--;
    long long int text_pos;
    bool is_amb;
    size_t n_amb=0, outd;

    //temporal files
    dbg_boss::label_t label(dbg_index.k-1);
    std::vector<uint8_t> syms(dna_alphabet::sigma*2);
    std::vector<size_type> r_b(dna_alphabet::sigma*2);
    std::vector<size_type> r_a(dna_alphabet::sigma*2);
    size_type tmp_node, tmp_node2, neighbor_node;

    std::map<size_type, bool> kmers_in_read;
    std::map<size_type, bool> neighbor_colored;
    text_pos=end;

    while(text_pos>start) {

        for(size_t j=dbg_index.k-1;j-->0;){
            label[j] = text[text_pos];
            text_pos--;
        }

        tmp_node = dbg_index.backward_search(label,
                                             0, syms,
                                             r_b, r_a)[0].r_start;
        outd = dbg_index.outdegree(tmp_node);

        if(outd>1){
            tmp_node2 = dbg_index.outgoing(tmp_node,
                                           text[text_pos],
                                           true, syms, r_b, r_a);
            for(size_t i=1;i<=outd;i++){
                neighbor_node = dbg_index.outgoing(tmp_node, i, false);
                if(neighbor_node!=tmp_node2 && color_marks[neighbor_node]){
                    neighbor_colored[color_marks_rs(neighbor_node)]=true;
                }
            }
        }

        kmers_in_read[color_marks_rs(tmp_node)]=true;
        is_amb=false;

        while(text[text_pos+1]!=1) {

            tmp_node = dbg_index.outgoing(tmp_node,
                                          text[text_pos],
                                          true, syms, r_b, r_a);
            if(color_marks[tmp_node]){

                if(kmers_in_read[color_marks_rs(tmp_node)]){
                    is_amb=true;
                }else{
                    kmers_in_read[color_marks_rs(tmp_node)]=true;
                }
            }

            outd = dbg_index.outdegree(tmp_node);
            if(outd>1 && dbg_index.solid_nodes[tmp_node]){
                tmp_node2 = dbg_index.outgoing(tmp_node,
                                              text[text_pos-1],
                                              true, syms, r_b, r_a);
                for(size_t i=1;i<=outd;i++){
                    neighbor_node = dbg_index.outgoing(tmp_node, i, false);
                    if(neighbor_node!=tmp_node2 && color_marks[neighbor_node]){
                        neighbor_colored[color_marks_rs(neighbor_node)]=true;
                    }
                }
            }
            text_pos--;
        }

        for(auto const& kmers: kmers_in_read){
            if(neighbor_colored[kmers.first]){
                is_amb = true;
                break;
            }
        }

        if(is_amb){
            n_amb++;
        }

        neighbor_colored.clear();
        kmers_in_read.clear();
        text_pos++;
    }

    try {
        //retrieve the get_colors that we cannot use for the current read
        std::lock_guard<std::mutex> lck(mtx);
        tot_amb+=n_amb;
    }catch (std::logic_error&){
        std::cout<<"some error in the threads"<<std::endl;
    }

}
