//
// Created by diediaz on 18-04-19.
//

#include <iostream>
#include <boss.hpp>

using namespace std;
using namespace sdsl;

const std::string currentDateTime() {
    time_t     now = time(nullptr);
    struct tm  tstruct={};
    char       buf[80];
    tstruct = *localtime(&now);
    strftime(buf, sizeof(buf), "%Y-%m-%d.%X", &tstruct);
    return buf;
}

#define LOG(msg) \
    std::cout << "[" << currentDateTime() << "]: " << msg << std::endl

int main(int argc, char* argv[]){

    if (argc < 5) {
        std::cerr << "Usage: " << argv[0] << " FASTX_FILE K N_TREADS OUTPUT_PREFIX" << std::endl;
        return 1;
    }

    size_t K= size_t(std::stoi(argv[2]));
    size_t n_threads= size_t(std::stoi(argv[3]));
    std::string input_fastq=argv[1];
    std::string output_prefx = argv[4];
    std::ofstream output_file;


    LOG("Creating colored BOSS index from file "<<argv[1]);
    dbg_boss cboss_index(input_fastq, K, n_threads);

    LOG("Storing the graph");
    store_to_file(cboss_index, output_prefx+".cboss");

    LOG("Computing coloring stats");
    std::pair<std::map<size_t, size_t>,
              std::map<size_t, size_t>> color_stats = cboss_index.color_stats();

    output_file.open(output_prefx + ".index_stats");
    output_file<<"#kmer size: "<<cboss_index.k<<std::endl;
    output_file<<"#Number of dBG nodes: "<<cboss_index.tot_nodes()<<std::endl;
    output_file<<"#Number of dBG edges: "<<cboss_index.tot_edges()<<std::endl;
    output_file<<"#Number of dBG solid nodes: "<<cboss_index.n_solid_nodes<<std::endl;
    output_file<<"#Number of colored dBG nodes: "<< cboss_index.tot_colored_nodes()<<
               " ("<<(double(cboss_index.tot_colored_nodes())/cboss_index.tot_nodes())*100<<"% of the dBG nodes)"<<std::endl;
    output_file<<"#Number of different colors: "<< cboss_index.tot_colors()<<std::endl;
    output_file<<"#Size in MB of colored dBG index: "<<sdsl::size_in_mega_bytes(cboss_index)<<std::endl;
    output_file<<"#Percentage of the index space used by the colors: "<< cboss_index.color_space_frac()*100<<std::endl;
    output_file<<"#Frequency of every color"<<std::endl;
    output_file<<"#Color\tfreq"<<std::endl;
    for(auto const& item : color_stats.first){
        output_file<<item.first<<"\t"<<item.second<<std::endl;
    }
    output_file<<"#Frencency of colored nodes"<<std::endl;
    output_file<<"#Number_of_colors_in_node\tfreq"<<std::endl;
    for(auto const& item : color_stats.second){
        output_file<<item.first<<"\t"<<item.second<<std::endl;
    }
    output_file.close();
    LOG("DONE");
    return 0;
}