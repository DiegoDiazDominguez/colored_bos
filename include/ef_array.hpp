//
// Created by Diego Diaz on 2019-05-07.
//

//elias-fano array

#ifndef COL_BOSS_EF_ARRAY_HPP
#define COL_BOSS_EF_ARRAY_HPP

#include <sdsl/rrr_vector.hpp>
#include <sdsl/int_vector.hpp>
#include <sdsl/int_vector_buffer.hpp>

template<class arr_t>
class ef_array {

public:
    typedef uint64_t size_type;

private:
    typedef sdsl::rrr_vector<63> comp_bv_t;
    typedef typename sdsl::rrr_vector<63>::select_1_type comp_bv_ss_t;

    comp_bv_t m_values_marks;
    comp_bv_ss_t m_values_marks_ss;
    sdsl::int_vector<64> m_values;
    size_t m_w=64;
    uint64_t m_size=0;
    uint32_t m_max_val=0;

public:
    explicit ef_array(arr_t& uncomp_values);
    ef_array()=default;
    size_type size() const;
    size_type max_val() const;
    size_type serialize(std::ostream& out, sdsl::structure_tree_node* v, std::string name) const;
    void load(std::istream&);
    size_type read_elm(size_type pos) const;
    void swap(ef_array<arr_t>& other);

private:
    void bits_write(uint64_t i, uint64_t j, uint64_t value, sdsl::int_vector_buffer<64>& buffer);
    size_type bits_read(size_type i, size_type j) const;
};

template<class arr_t>
ef_array<arr_t>::ef_array(arr_t &uncomp_values) {

    size_t width;
    uint64_t curr_pos=0, value;
    m_size = uncomp_values.size();

    sdsl::int_vector_buffer<64> values_buffer("ef_buffer.sdsl",
                                              std::ios::out,
                                              1024*1024,
                                              false);

    sdsl::int_vector_buffer<1> bv_buffer("ef_buffer_bv.sdsl",
                                         std::ios::out,
                                         1024*1024,
                                         false);

    for(size_type i=0;i<uncomp_values.size();i++){

        //2 is the smallest number allowed
        value = uncomp_values[i]+2;

        if((value-2)>m_max_val) m_max_val = value-2;

        width =  (m_w-1)-__builtin_clzll(value);
        bits_write(curr_pos,
                   curr_pos+width-1,
                   value & ~(1ULL << width),
                   values_buffer);
        bv_buffer[curr_pos] = true;
        curr_pos+=width;
    }
    bv_buffer[curr_pos] = true;

    /*std::cout<<"values"<<std::endl;
    for(size_type i=0;i<values_buffer.size();i++){
        std::cout<<values_buffer[i]<<" ";
    }
    std::cout<<" "<<std::endl;*/

    values_buffer.close();
    bv_buffer.close();

    sdsl::int_vector<64> values;
    sdsl::load_from_file(m_values, "ef_buffer.sdsl");

    sdsl::bit_vector tmp_bv;
    sdsl::load_from_file(tmp_bv, "ef_buffer_bv.sdsl");

    sdsl::rrr_vector<63> tmp_bv_comp(tmp_bv);
    m_values_marks.swap(tmp_bv_comp);

    /*for(size_type i=0;i<m_values.size();i++){
        std::cout<<m_values[i];
    }
    std::cout<<""<<std::endl;*/

    m_values_marks_ss.set_vector(&m_values_marks);

    if(remove("ef_buffer.sdsl") != 0) {
        perror("Error deleting file");
    }

    if(remove("ef_buffer_bv.sdsl") != 0) {
        perror("Error deleting file");
    }
}

template<class arr_t>
inline void ef_array<arr_t>::bits_write(uint64_t i,
                                        uint64_t j,
                                        uint64_t value,
                                        sdsl::int_vector_buffer<64>& buffer) {

    size_t cell_i, cell_j;
    cell_i = i/m_w;
    cell_j = j/m_w;

    if(cell_i>=buffer.size()) buffer[cell_i] = 0;
    if(cell_i==cell_j){
        buffer[cell_j] = buffer[cell_j] & ~(((1ULL<<(j-i+1)) -1) << (i & (m_w-1)));
        buffer[cell_j] = buffer[cell_j] | value << (i & (m_w-1));
        //std::cout<<buffer[cell_j]<<std::endl;
    }else{
        if(cell_j>=buffer.size()) buffer[cell_j] = 0;
        buffer[cell_i] = (buffer[cell_i] & ((1ULL<<(i & (m_w-1)))-1)) | (value << (i & (m_w-1)));
        buffer[cell_j] = (buffer[cell_j] & ~((1ULL<<((j+1) & (m_w-1)))-1)) |
                           (value >> (m_w-(i & (m_w-1))));
    }
}

template<class arr_t>
inline uint64_t ef_array<arr_t>::bits_read(ef_array::size_type i, ef_array::size_type j) const {
    size_t cell_i, cell_j;
    cell_i = i/m_w;
    cell_j = j/m_w;

    if(cell_i == cell_j){
        return (m_values[cell_j] >> (i & (m_w-1))) & ((1ULL<<(j-i+1))-1);
    }else{
        return (m_values[cell_i] >> (i & (m_w-1) )  |
                (m_values[cell_j] & ((1ULL <<((j+1) & (m_w-1)))-1)) << (m_w - (i & (m_w-1))));
    }
}

template<class arr_t>
inline uint64_t ef_array<arr_t>::read_elm(ef_array::size_type pos) const {
    assert(pos<m_size);
    size_type i= m_values_marks_ss(pos+1);
    size_type j= m_values_marks_ss(pos+2)-1;
    size_type elm = bits_read(i,j);

    return (1U<<(j-i+1U) | elm)-2;
}

template<class arr_t>
uint64_t ef_array<arr_t>::serialize(std::ostream &out, sdsl::structure_tree_node *v, std::string name) const {

    sdsl::structure_tree_node* child = sdsl::structure_tree::add_child(v, name, sdsl::util::class_name(*this));
    size_t written_bytes = 0;
    written_bytes += m_values.serialize(out,child, "m_values");
    written_bytes += m_values_marks.serialize(out, child, "m_values_marks");
    written_bytes += m_values_marks_ss.serialize(out, child, "m_values_marks_ss");
    written_bytes += write_member(m_size, out, child, "m_size");
    written_bytes += write_member(m_max_val, out, child, "m_max_val");
    sdsl::structure_tree::add_size(child, written_bytes);
    return written_bytes;
}

template<class arr_t>
void ef_array<arr_t>::load(std::istream & in) {
    m_values.load(in);
    m_values_marks.load(in);
    m_values_marks_ss.load(in, &m_values_marks);
    sdsl::read_member(m_size, in);
    sdsl::read_member(m_max_val, in);
}

template<class arr_t>
inline uint64_t ef_array<arr_t>::size() const {
    return m_size;
}

template<class arr_t>
inline uint64_t ef_array<arr_t>::max_val() const {
    return m_max_val;
}

template<class arr_t>
void ef_array<arr_t>::swap(ef_array<arr_t> &other) {
    m_values.swap(other.m_values);
    m_values_marks.swap(other.m_values_marks);
    m_values_marks_ss.swap(other.m_values_marks_ss);
    m_values_marks_ss.set_vector(&m_values_marks);
    std::swap(m_size,other.m_size);
    std::swap(m_max_val,other.m_max_val);
}

#endif //COL_BOSS_EF_ARRAY_HPP
